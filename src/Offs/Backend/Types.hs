{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : Offs.Backend.Types
Description : Data type definitions we use to represent the "outside world"
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

This module defines the various types that we use to model the data we are
manipulating. It also defines the lexing rules from a CSV line to our
representation.
-}
module Offs.Backend.Types where

import           Protolude

import qualified Data.ByteString.Char8 as BC
import           Data.Csv
import qualified Data.List             as L
import qualified Data.Text.Lazy        as TL
import           Data.Time.Calendar
import           Data.Time.Clock
import           Data.Time.Format
import qualified Data.Vector           as V
import           Text.ICalendar.Types

-- | Pretty-printable stuff - no need for the big libraries here.
class Printable a where
    pp :: a -> TL.Text

-- | Useful for type checking, unwrapped at compile time.
newtype Officer = Officer { name :: TL.Text }

-- | Used to print event summaries.
instance Printable Officer where
    pp = name

-- | Data type representing a parsing of the file: it's either a global error
-- or a list of results (per-line), and these might have a result, an error, or
-- both. This can occur if we find a night officer but no day officer in a line
-- that should've contained one for example. This allows to show all parsing
-- errors in one run.
type Parsing = Either Text SuccessfulParsing
type SuccessfulParsing = [(Maybe Text, Maybe (UTCTime -> VEvent))]

-- | The time periods we consider.
data Shift  = Daytime
            | Nighttime
            | WE

-- | Mainly used for event UUIDs.
instance Printable Shift where
    pp Daytime   = "jour"
    pp Nighttime = "nuit"
    pp WE        = "weekend"

-- | A day in a firebrigade... A date, and who's responsible when. The numbers
-- represent the index of the officer in the officer list found in the file.
data Service = Service
    { date  :: Day
    , jourM :: Maybe Int
    , nuitM :: Maybe Int
    }

-- | How to parse a line into a 'Service'.
instance FromRecord Service where
    parseRecord r =
        Service <$> r.! 0 <*> findDayOff r <*> findNightOff r

-- | How to parse the first field into a date
instance FromField Day where
    parseField = parseTimeM True chTimeLocale "%A %d.%m.%y" . BC.unpack

-- | Get the indices of the fields containing the "Of de Serv" string.
findOffs :: Record -> [Int]
findOffs = L.elemIndices "Of de Serv" . V.toList

-- | Find out the officer (if there is one) for days.
findDayOff :: Record -> Parser (Maybe Int)
findDayOff xs = case filter (\x -> x `mod` 2 == 0) . findOffs $ xs of
    []  -> pure Nothing
    [x] -> pure . Just $ (x - 2) `div` 2
    _   -> pure Nothing

-- | Find out the officer (if there is one) for nights.
findNightOff :: Record -> Parser (Maybe Int)
findNightOff xs = case filter (\x -> x `mod` 2 == 1) . findOffs $ xs of
    []  -> pure Nothing
    [x] -> pure . Just $ (x - 3) `div` 2
    _   -> pure Nothing

chTimeLocale :: TimeLocale
chTimeLocale = TimeLocale
    { wDays  = [ ("dimanche", "dim"), ("lundi", "lun"), ("mardi", "mar"), ("mercredi", "mer"), ("jeudi", "jeudi")
               , ("vendredi", "ven"), ("samedi", "sam")
               ]
    , months = [ ("janvier", "jan"), ("février", "fev"), ("mars", "mar"), ("avril", "avr"), ("mai", "mai")
               , ("juin", "juin"), ("juillet", "juil"), ("août", "août"), ("septembre", "sept"), ("octobre", "oct")
               , ("novembre", "nov"), ("décembre", "déc")
               ]
    , amPm = ("am", "pm")
    , dateTimeFmt = "%A %d/%m/%y %T"
    , dateFmt = "%A %d/%m/%Y"
    , timeFmt = "%T"
    , time12Fmt = "%T"
    , knownTimeZones = []
    }

