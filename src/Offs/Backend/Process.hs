{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{- HLINT ignore "Use String" -}

{-|
Module      : Offs.Backend.Process
Description : Do the processing on the parsed data.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

This module exposes functions that expect the lexed data from the CSV (from
"Types") and do the transformations needed to hand it to the "Events" functions
who create the actual ICS.
-}

module Offs.Backend.Process
    (processFile)
where

import           Protolude

import           Offs.Backend.Events
import           Offs.Backend.Types

import qualified Data.ByteString.Lazy as BL
import           Data.Csv
import qualified Data.List            as L
import qualified Data.Text            as T
import qualified Data.Text.Lazy       as TL
import           Data.Time.Calendar
import           Data.Time.Clock
import           Data.Time.Format
import qualified Data.Vector          as V
import           Text.ICalendar.Types
import           Text.Regex.Base
import           Text.Regex.TDFA.Text

-- | Takes a list of lines and returns a 'Parsing' of the data it contains.
processFile :: [Text] -> Parsing
processFile = parseData . parseLimits

-- | Used to read the last day date and last night date from the file,
-- alongside the list of officers. Returns a global error or the tail of the
-- data with the parsed informations.
parseLimits :: [Text] -> Either Text (Day, Day, [Officer], [Text])
parseLimits input = do
    night <- parseEndDate input 3
    day   <- parseEndDate input 6
    offs  <- parseOfficers input
    return (night, day, offs, drop 11 input)

-- | Use regular expressions to extract dates from the "latest valid date" field in the CSV.
parseEndDate :: [Text]          -- ^ Input data.
             -> Int             -- ^ Line at which to look for the date.
             -> Either Text Day
-- I know, I know, this is ugly. But regexes are never pretty anyway...
parseEndDate input line =
    let reg = "[0-9]{2}/[0-9]{2}/[0-9]{2}" :: Text in
    let rez = mrMatch (match (makeRegex reg :: Regex) (input L.!! line) :: MatchResult Text) in
    if T.null rez then Left $ T.pack "Could not match date for end of day/night calendar."
                  else parseTimeM True defaultTimeLocale "%d/%m/%y" (T.unpack rez)

-- | Extract a list of present officers from the data. Assumes the list is on
-- line 12 of the CSV.
parseOfficers :: [Text] -- ^ Input data.
              -> Either Text [Officer]
parseOfficers = Right . fmap (Officer . TL.fromStrict)
              . filter (not . T.null) . T.splitOn "," . (L.!! 11)

-- | If we already have a global error, propagate it. Otherwise find the
-- officers for each shift and create the events.
parseData :: Either Text (Day, Day, [Officer], [Text]) -> Parsing
parseData (Left err)                                  = Left err
parseData (Right (nightLimit, dayLimit, offs, input)) = Right (input >>= transf . decode')
    where
          -- | Map the text into our internal typed representation.
          decode' :: Text -> Either [Char] (V.Vector Service)
          decode' = decode NoHeader . BL.fromStrict . encodeUtf8

          -- | Transform our internal representation into a list of potential
          -- errors and potential events, accounting for a lexing error.
          transf :: Either [Char] (V.Vector Service) -> SuccessfulParsing
          transf = either (\err -> [(filterErrs err, Nothing)]) (serviceToEvent . V.head)

          -- | Filter our meaningless errors due to empty fields being present.
          -- Probably the one truely ugly function of the project.
          filterErrs :: [Char] -> Maybe Text
          filterErrs err = let err' = T.pack err in
            if T.isInfixOf "parseTimeM: no parse of \"\"" err'
                || T.isInfixOf "parseTimeM: no parse of \"Nombre" err'
            then Nothing
            else Just err'

          -- | Map a valid internal representation into events and errors.
          serviceToEvent :: Service -> SuccessfulParsing
          serviceToEvent serv
            | date serv <= dayLimit   = if date serv <= nightLimit then both serv else dayOnly serv
            | date serv <= nightLimit = nightOnly serv
            | otherwise               = []

          -- | Locate a day and a night officer.
          both :: Service -> SuccessfulParsing
          both serv = dayOnly serv ++ nightOnly serv

          -- | Locate only a day officer.
          dayOnly :: Service -> SuccessfulParsing
          dayOnly serv = case jourM serv of
            Just jour ->
                if jour >= length offs
                then [(Just ("Officier de jour invalide pour " <> (T.pack . show . date $ serv)), Nothing)]
                else [(Nothing, Just $ dayOfficerEvent (date serv) (offs L.!! jour ))]
            Nothing ->
                [(Just ("Officier de jour invalide pour " <> (T.pack . show . date $ serv)), Nothing)]

          -- | Locate only a night officer.
          nightOnly :: Service -> SuccessfulParsing
          -- FIXME: factor out this and the above function, this is inelegant.
          nightOnly serv = case nuitM serv of
            Just nuit ->
                if nuit >= length offs
                then [(Just ("Officier de nuit invalide pour " <> (T.pack . show . date $ serv)), Nothing)]
                else [(Nothing, Just $ nightOfficerEvent (date serv) (offs L.!! nuit ))]
            Nothing ->
                [(Just ("Officier de nuit invalide pour " <> (T.pack . show . date $ serv)), Nothing)]
