{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : Offs.Backend.Events
Description : Templates for events inserted in the calendar.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

This module defines templates for building the events we insert into the final
ICalendar, strictly adhering to RFC5545.
-}

module Offs.Backend.Events
    ( dayOfficerEvent
    , nightOfficerEvent
    , buildCalendar
    ) where

import           Protolude

import           Offs.Backend.Types

import qualified Data.ByteString.Lazy as BSL
import           Data.CaseInsensitive
import           Data.Default
import           Data.Map.Strict      as M
import           Data.Set             as S
import qualified Data.Text.Lazy       as T
import           Data.Time.Calendar
import           Data.Time.Clock
import           Data.Time.LocalTime
import           Text.ICalendar.Types

-- | Write a an RFC5545 'VEvent' for a day shift for the given officer.
dayOfficerEvent :: Day      -- ^ The date for which this event happens.
                -> Officer  -- ^ The officer who's shift it is.
                -> UTCTime  -- ^ The current time (required for DTSTAMP by RFC5545 3.8.7.2).
                -> VEvent
dayOfficerEvent = templateEvent Daytime

-- | Write a an RFC5545 'VEvent' for a night shift for the given officer.
nightOfficerEvent :: Day      -- ^ The date for which this event happens.
                  -> Officer  -- ^ The officer who's shift it is.
                  -> UTCTime  -- ^ The current time (required for DTSTAMP by RFC5545 3.8.7.2).
                  -> VEvent
nightOfficerEvent = templateEvent Nighttime

-- | Build an RFC5545 'VCalendar' from a list of events.
buildCalendar :: [UTCTime -> VEvent] -- ^ The list of 'VEvents' to insert into the 'VCalendar'
              -> UTCTime             -- ^ The current time to write the DTSTAMPs.
              -> VCalendar
buildCalendar events now = def { vcTimeZones = M.singleton zurichTimeZone (buildTimeZone zurichTimeZone)
                               , vcEvents = M.fromList $ fmap (makeMappable . ($ now)) events
                               , vcOther = S.singleton $ OtherProperty (mk "X-WR-CALNAME") calname (OtherParams S.empty)
                               }
    where makeMappable :: VEvent -> ((T.Text, Maybe (Either Date DateTime)), VEvent)
          makeMappable event = ((uidValue . veUID $ event, Nothing), event)

          calname :: BSL.ByteString
          calname = "Off Service SDIS Chamberonne"

-- | Template 'VEvent' builder.
templateEvent :: Shift   -- ^ When in the day is the shift occuring.
              -> Day     -- ^ The date for this shift.
              -> Officer -- ^ The officer who's shift it is.
              -> UTCTime -- ^ The current time (required for DTSTAMP by RFC5545 3.8.7.2).
              -> VEvent
templateEvent eventShift eventDate off now =
    VEvent { veDTStamp       = DTStamp now def
           , veUID           = UID buildUID def
           , veClass         = def
           , veDTStart       = Just $ DTStartDateTime buildDTStart def
           , veCreated       = Nothing
           , veDescription   = Nothing
           , veGeo           = Nothing
           , veLastMod       = Nothing
           , veLocation      = Nothing
           , veOrganizer     = Nothing
           , vePriority      = def
           , veSeq           = def
           , veStatus        = Nothing
           , veSummary       = Just $ Summary (mconcat [pp off, " - ", pp eventShift]) Nothing Nothing def
           , veTransp        = def
           , veUrl           = Nothing
           , veRecurId       = Nothing
           , veRRule         = S.empty
           , veDTEndDuration = Just . Left $ DTEndDateTime buildDTEnd def
           , veAttach        = S.empty
           , veAttendee      = S.empty
           , veCategories    = S.empty
           , veComment       = S.empty
           , veContact       = S.empty
           , veExDate        = S.empty
           , veRStatus       = S.empty
           , veRelated       = S.empty
           , veResources     = S.empty
           , veRDate         = S.empty
           , veAlarms        = S.empty
           , veOther         = S.empty
           }
    where buildUID = mconcat [ pp eventShift
                             , T.pack . showGregorian $ eventDate
                             , "@sdis-chambronne.ch"
                             ]

          buildDTStart = ZonedDateTime { dateTimeFloating = LocalTime eventDate $ case eventShift of Daytime   -> TimeOfDay 6 0 0
                                                                                                     Nighttime -> TimeOfDay 18 0 0
                                       , dateTimeZone     = zurichTimeZone
                                       }

          buildDTEnd = ZonedDateTime { dateTimeFloating = case eventShift of Daytime   -> LocalTime eventDate (TimeOfDay 18 0 0)
                                                                             Nighttime -> LocalTime (addDays 1 eventDate) (TimeOfDay 6 0 0)
                                     , dateTimeZone = zurichTimeZone
                                     }

-- | IETF is sitting on it's ass about this, so we need to actually build the
-- TZ with correct DST and shit. Information taken from my own calendar systems
-- which I trust.
buildTimeZone :: T.Text     -- ^ Name of the create TZone.
              -> VTimeZone
buildTimeZone tzname = VTimeZone { vtzId        = TZID tzname False def
                                 , vtzLastMod   = Nothing
                                 , vtzUrl       = Nothing
                                 , vtzStandardC = S.singleton zurichStandard
                                 , vtzDaylightC = S.singleton zurichDaylight
                                 , vtzOther     = S.empty
                                 }

-- | Same as above, this technically only needs to be unique, but we're keeping
-- along conventions.
zurichTimeZone :: T.Text
zurichTimeZone = "Europe/Zurich"

-- | DST for CEST in 2019. FIXME: How the hell do calendar apps generate these
-- for every year ?
zurichDaylight :: TZProp
zurichDaylight = TZProp
               { tzpDTStart      = DTStartDateTime (FloatingDateTime $ LocalTime (fromGregorian 2020 03 29) (TimeOfDay 3 0 0)) def
               , tzpTZOffsetTo   = UTCOffset 7200 def
               , tzpTZOffsetFrom = UTCOffset 3600 def
               , tzpRRule        = S.empty
               , tzpComment      = S.empty
               , tzpRDate        = S.empty
               , tzpTZName       = S.singleton $ TZName "CEST" (Just . Language $ mk "EN") def
               , tzpOther        = S.empty
               }

-- | ST for CEST in 2018. FIXME: same as 'zurichDaylight'.
zurichStandard :: TZProp
zurichStandard = TZProp
               { tzpDTStart      = DTStartDateTime (FloatingDateTime $ LocalTime (fromGregorian 2020 10 25) (TimeOfDay 3 0 0)) def
               , tzpTZOffsetTo   = UTCOffset 3600 def
               , tzpTZOffsetFrom = UTCOffset 7200 def
               , tzpRRule        = S.empty
               , tzpComment      = S.empty
               , tzpRDate        = S.empty
               , tzpTZName       = S.singleton $ TZName "CET" (Just . Language $ mk "EN") def
               , tzpOther        = S.empty
               }
