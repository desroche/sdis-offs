{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE TypeOperators         #-}

{-|
Module      : Offs.Webapp.API
Description : The API for serving sdis-offs.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX
-}

module Offs.Webapp.API where

import           Protolude

import           Offs.Backend.Events
import           Offs.Backend.Process
import           Offs.Backend.Types
import           Offs.Webapp.Homepage

import qualified Data.ByteString.Lazy       as BSL
import qualified Data.ByteString.Lazy.Char8 as BLC
import           Data.Default
import qualified Data.Text                  as T
import           Data.Text.Encoding.Error
import qualified Data.Text.Lazy.Encoding    as TE
import           Data.Time.Clock
import           Network.HTTP.Media
        ( MediaType
        , (//)
        , (/:)
        )
import           Servant
import           Servant.HTML.Blaze
import           Servant.Multipart
import           Text.Blaze.Html5
        ( Html
        )
import           Text.ICalendar.Printer
import           Text.ICalendar.Types

-- | Type-level description of our API.
--
-- A user can either get the homepage with instructions, or upload a file with the
-- given form, wich will give him back the converted file.
type OffsAPI = Get '[HTML] Html
          :<|> MultipartForm Mem (MultipartData Mem) :> Post '[CalendarFile] (MyHeaders VCalendar)


-- | Datatype for text/calendar mime type.
data CalendarFile

-- | Headers added "by hand" to set the filename during download.
type MyHeaders a = Headers '[Header "Content-Disposition" Text] a

-- | Mimetype for our datatype.
instance Accept CalendarFile where
    contentType _ = "text" // "calendar" /: ("charset", "utf-8")

-- | How to convert a CalendarFile VCalendar to a ByteString.
instance MimeRender CalendarFile VCalendar where
    mimeRender _ = printICalendar def

-- | Main webserver loop, replying to queries it recieves.
server :: Server OffsAPI
server = return homepage
    :<|> processUpload

-- | Takes a file uploaded through the homepage form and converts is to the
-- ICS, failing gracefully on error.
processUpload :: MultipartData Mem -> Handler (MyHeaders VCalendar)
processUpload multipData = case head $ files multipData of
    Nothing     -> throwError err400 { errBody = "Le fichier à convertir doit être fourni!" }
    Just upload ->
        either (convErr . unwrapErr) (parsingToHandler . processFile . T.lines . toStrict) (TE.decodeUtf8' . fdPayload $ upload)

          -- | Go from our parsing datatype to the Handler Monad (if Parsing were a Monad, this would be a NT)
    where parsingToHandler :: Parsing -> Handler (MyHeaders VCalendar)
          parsingToHandler = either convErr sendFile

          -- | Return the file.
          sendFile :: SuccessfulParsing -> Handler (MyHeaders VCalendar)
          sendFile xs = let (errs, rezs) = unzip xs in
              if any isJust errs then convErr . T.unlines . catMaybes $ errs
              else liftIO $ (addHeader filename . buildCalendar (catMaybes rezs)) <$> getCurrentTime

          -- | Content of the "Content-Disposition" header giving the filename to the browser.
          filename :: Text
          filename = "attachement; filename=\"calendrier_off_service.ics\""

          -- | Convert an error from our parsing to a HTTP 400 error.
          convErr :: Text -> Handler (MyHeaders VCalendar)
          convErr err = throwError err400 { errBody = TE.encodeUtf8 . fromStrict $ err }

          -- | Unwrap a decoding exception to text for a HTTP 400 error.
          unwrapErr :: UnicodeException -> Text
          unwrapErr _ = "Invalid file: non UTF-8 encoding detected!"
