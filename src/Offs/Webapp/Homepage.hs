{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : Offs.Webapp.Homepage
Description : This module contains the code to generate the HTML for the homepage.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX

This module contains the code for the generation of the homepage, written in the "Text.Blaze.Html5" EDSL.
-}

module Offs.Webapp.Homepage where

import Text.Blaze.Html5            as H
import Text.Blaze.Html5.Attributes as A

-- | The homepage that serves as an instruction manual, and a post page for
-- people using browsers.
homepage :: Html
homepage = docTypeHtml $ do
    H.head $
             H.title "Calendrier Offs de service"
    H.body $ do
             h1 "SDIS-OFFS"

             h2 "Conversion"
             H.form ! A.method "POST" $ do
                table . tr $ do
                         td "Choisir le fichier à convertir"
                         td $ input ! A.type_ "file" ! A.name "file" ! A.required "file"
                input ! A.type_ "submit" ! A.formenctype "multipart/form-data"

             h2 "Details"
             p $ do
                "Ce programme permets de convertir le fichier CSV obtenu à "
                "partir du tableau des officiers de service en un ICS importable "
                "par Google Calendar."

             p $ do
                "On attends un fichier CSV valide encodé en UTF-8, suivant les règles "
                "énoncées dans le "
                a ! href "https://gitlab.gnugen.ch/desroche/sdis-offs/blob/master/README.md" $ "README"

             p $ do
                "See the "
                a ! href "https://gitlab.gnugen.ch/desroche/sdis-offs" $ "gitlab page"
                " for more information on the implementation of this service."
             p  " This service is provided with no guarantees of any kind."

             footer $ do
                      "SDIS-Offs calendar v2, BSD3 licenced. Written in "
                      a ! href "https://haskell.org" $ "haskell"
                      " using "
                      a ! href "https://hackage.haskell.org/package/servant" $ "servant"
                      " by J. Desroches for SDIS Chamberonne, 2018."

