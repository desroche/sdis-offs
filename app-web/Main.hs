{-# LANGUAGE NoImplicitPrelude #-}

module Main where

import Protolude

import Offs.Webapp.API

import Network.Wai.Handler.Warp
import Network.Wai.Parse
import Servant
import Servant.Multipart
import qualified Options.Applicative as O

-- | Main entry point to the program. Parse the given options to get the port
-- on which to listen if the default (8080) is not to be used.
main :: IO ()
main = O.execParser opts >>= \(PortOption port) ->
    run port $ serveWithContext offsAPI myContext server

    where offsAPI   = Proxy :: Proxy OffsAPI
          myContext = myMultipartOptions :. EmptyContext

          myMultipartOptions :: MultipartOptions Mem
          myMultipartOptions = (defaultMultipartOptions Proxy)
            { generalOptions = setMaxRequestFileSize 104857600 defaultParseRequestBodyOptions }

          opts = O.info (portOption <**> O.helper)
            ( O.fullDesc
           <> O.progDesc "Convert the officier de service CSV to an ICS"
           <> O.header "sdis-offs - a csv to ics converter" )

-- | The possible options on the CLI.
newtype PortOption = PortOption { port :: Int }

-- | Parser for the options.
portOption :: O.Parser PortOption
portOption = PortOption <$> O.option O.auto
    ( O.long "port"
   <> O.short 'p'
   <> O.help "Port on which to listen."
   <> O.showDefault
   <> O.value 8080
   <> O.metavar "INT" )
