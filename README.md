# sdis-offs v.2.1.1

Ce programme permets de convertir le fichier CSV obtenu à partir du tableau des
officiers de service en un fichier ICS, importable dans un calendrier.

## Utilisation

Deux programmes sont crées à la compilation: un serveur web et un binaire pour faire la
conversion localement.

### Version web

Le programme web est un éxecutable qui écoute par défaut sur le port 8080, et
peut optionnellement recevoir l'argument `-p|--port` pour modifier le port
d'écoute. Quand on lui parle en HTTP, il va répondre avec une page HTML avec un
formulaire pour uploader un fichier CSV, qu'il convertira et renverra. Dans le
cas ou une ou plusieurs erreurs se produisent, le programme renverra une HTTP
400 avec la liste des erreurs produites.

### Version locale

Le programme s'attend à trouver dans le dossier où il est exécuté un fichier
nommé `input.csv` comme données d'entrées. Il créera à son tour deux fichiers,
un nommé `errors.txt` qui contient les dates auxquelles aucun ou plusieurs
officiers de service ont été trouvés pour la même plage horaire, et un fichier
`output.ics`.

## Règles de format d'entrée

Quelques règles sont considérées comme toujours vraies dans le fichier:
0
 * C'est un CSV valide encodé en UTF-8 séparé par des virgules
 * La ligne 12 du fichier excel contient une liste des officiers
 * Les lignes "... planning validé ..." contiennent une date au format
   JJ/MM/AA, la cellule G4 celui de nuit et G7 de jour.
 * Chaque ligne avec une date doit commencer par une date formattée JOUR
   JJ.MM.AA, où JOUR est le nom du jour en français.
 * C'est exactement le texte "Of de Serv" qui sers à détecter si cette case
   désigne un officier de service, donc il faut veiller à ne pas modifier
   celui-ci.

En gros, gardez le fichier tel quel (en faisant attention aux deux formats de
date différents) et tout ira bien :p.

## Format de sortie

Le fichier sorti est un ICS valide selon la RFC 5545. Il est importable
à volonté et mettra à jour les évenements comme il faut grâce à un ID
consistent (`(nuit|jour)yyyy-mm-jj@sdis-chamberonne.ch`) et unique pour chaque
shift.
