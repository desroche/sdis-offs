{-# LANGUAGE NoImplicitPrelude #-}
{-# LANGUAGE OverloadedStrings #-}
{-|
Module      : Main
Description : Build executable that runs on local files.
Copyright   : (c) Joachim Desroches, 2018
License     : BSD3
Maintainer  : joachim.desroches@epfl.ch
Stability   : experimental
Portability : POSIX
-}

module Main where

import           Protolude

import           Offs.Backend.Events
import           Offs.Backend.Process
import           Offs.Backend.Types

import qualified Data.ByteString.Lazy   as BLS
import           Data.Default
import qualified Data.Text              as T
import           Data.Time.Clock
import           System.IO.Error
import           Text.ICalendar.Printer
import           Text.ICalendar.Types

-- | Entry point for the program. Reads the input, splits it, processes it and
-- spits the output back.
main :: IO ()
main = readFile "input.csv" >>= parsingToIO . processFile . T.lines

    where parsingToIO :: Parsing -> IO ()
          parsingToIO = either (ioError . userError . T.unpack) writeOutput

-- | Splits the recieved processed data into and error stream and a result
-- stream, and writes them where they should go.
writeOutput :: SuccessfulParsing -- ^ The data to write out.
            -> IO ()
writeOutput xs = let (errs, rezs) = unzip xs in
    writeFile "errors.txt" (T.unlines $ catMaybes errs) >>
    writeCalendar (catMaybes rezs) >>= BLS.writeFile "calendrier_off_service.ics" . printICalendar def

-- | Timestamps the creation of the ICS (used for updating purposes).
writeCalendar :: [UTCTime -> VEvent] -> IO VCalendar
writeCalendar cal = buildCalendar cal <$> getCurrentTime
